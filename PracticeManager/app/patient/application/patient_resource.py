from flask import request, jsonify, Blueprint
from flask import current_app as app

from patient.domain.patient import Patient

patient_bp = Blueprint('patient', __name__)

@patient_bp.route('/patients', methods=['POST'])
def create_patient():
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        created_patient = patient_service.create_patient(Patient(**patient_data))

        return jsonify(created_patient.to_dict()), 201
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@patient_bp.route('/patients/<int:patient_id>', methods=['GET'])
def get_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient = patient_service.get_patient_by_id(patient_id)

        if not patient:
            return jsonify({'error': 'Patient not found'}), 404

        return jsonify(patient.to_dict())
    except Exception as e:
        return jsonify({'error': str(e)}), 400