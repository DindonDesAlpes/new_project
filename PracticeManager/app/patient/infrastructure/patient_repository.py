from patient.domain.patient import Patient
from patient.infrastructure.patient_entity import PatientEntity


class PatientRepository:
    def __init__(self):
        self.db = {}
        self.next_patient_id = 1

    def add_patient(self, patient: Patient):
        patient_id = self.next_patient_id
        self.next_patient_id += 1

        patient_entity = PatientEntity(
            id=patient_id,
            first_name=patient.first_name,
            last_name=patient.last_name,
            date_of_birth=patient.date_of_birth,
            social_security_number=patient.social_security_number
        )

        self.db[patient_id] = patient_entity

        return patient_entity
    
    def get_patient_by_id(self, patient_id):
        return self.db.get(patient_id)