# TP3 - Patient API

## Run tests using CLI
```bash
python -m unittest tests.patient.application.test_patient_service 
```

## Launch the application
```bash
 flask run
```

## A curl example to create a patient:
```bash
curl -v -X POST -H "Content-Type: application/json" \
  -d '{"first_name": "John", "last_name": "Doe", "date_of_birth": "1980-01-01", "social_security_number": 123456789012345}' \
  http://127.0.0.1:5000/patients
```